#!/bin/sh
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "This script sets the environment variables in docker build env"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

e=

# Get the environment from argument
while [ "$1" != "" ]; do
  case $1 in
    -e )
    shift
    e=$1
    ;; 
    * )                     
    usage
    exit 1
    esac
    shift
done


env_file="environments/${e}.env"

if [ ! -f $env_file ]; then
  echo -n "File $env_file does not exists"
  exit
fi

# Build the image
docker-compose build --force-rm deploy-${e}-aws-s3

# Push the image to AWS S3
docker-compose run --rm deploy-${e}-aws-s3
