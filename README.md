# React Docker Base

The repository contains a base for a dockerized version of a React app using the Create React App generator. The repository can be used as a base to develop a react app without the need to install any additional tools. Furthermore, if valid AWS IAM credentials are provided in the environment file running `deploy-aws-s3.sh` will push the build to AWS S3 bucket.

## Run locally for development

`docker-compose up app`

## Deploy on AWS S3 - Staging

`./deploy-aws-s3.sh -e staging`

## Deploy on AWS S3 - Production

`./deploy-aws-s3.sh -e production`

## Requirments and Dependencies

1. Docker and Docker Compose is installed and configured.
    * [Docker](https://docs.docker.com/install/)
    * [Docker Compose](https://docs.docker.com/compose/install/)

2. Before deploying on AWS S3 make sure to add required environment variables in `environemnts/<env>.env` files. Sample file `environment/sample.env.example`.

3. Find and replace `react-docker-base` to an appropriate application name.

4. Create an AWS S3 bucket with static website support and AWS CloudFront. Add the S3 bucket name and CloudFront Distribution ID to the environment file.

## Credits and References

1. [Dockerizing a React App](http://mherman.org/blog/2017/12/07/dockerizing-a-react-app/#.WwSPmxy-nVg)

2. [AWS CLI Getting Started Guide](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)

3. [Hosting a Static Website on Amazon S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)