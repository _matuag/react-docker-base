#!/bin/sh

source .env

# Configure AWS CLI
aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID}
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set region ${AWS_DEFAULT_REGION}

# copy the contents of /build to S3
aws s3 cp --recursive --acl public-read ./build s3://${AWS_S3_DEPLOY_BUCKET}/

# set the cache-control headers for index.html to prevent
# browser caching
aws s3 cp --acl public-read --cache-control="max-age=0, no-cache, no-store, must-revalidate" ./build/index.html s3://${AWS_S3_DEPLOY_BUCKET}/

# invalidate the CloudFront cache for index.html and service-worker.js
# to force CloudFront to update its edge locations with the new versions
aws cloudfront create-invalidation --distribution-id ${AWS_CF_DISTRIBUTION_ID} --paths /index.html
